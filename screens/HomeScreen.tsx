import React, { useState, useEffect, useCallback } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import { OutlinedTextField } from 'rn-material-ui-textfield';


import { useLazyQuery, gql } from '@apollo/client';

const GET_REPOSITORIES = gql`
query Repositories($search: String!) {
    search(first: 30, type: REPOSITORY, query: $search) {
      edges {
        cursor
        node {
          ...RepositoryData
        }
      }
    }
  }
  
  fragment RepositoryData on Repository {
    nameWithOwner
    shortDescriptionHTML
    descriptionHTML
    owner {
      avatarUrl
    }
    labels(first: 10) {
      edges {
        node {
          name
          color
        }
      }
    }
  }
`;

const GET_MORE_REPOSITORIES = gql`
query RepositoriesLazy($search: String!, $after: String!) {
    search(first: 30, type: REPOSITORY, query: $search, after: $after) {
      edges {
        cursor
        node {
          ...RepositoryDataLazy
        }
      }
    }
  }
  
  fragment RepositoryDataLazy on Repository {
    nameWithOwner
    shortDescriptionHTML
    descriptionHTML
    owner {
      avatarUrl
    }
    labels(first: 10) {
      edges {
        node {
          name
          color
        }
      }
    }
  }
`;

const HomeScreen = ({ navigation }) => {
  const [search, setSearch] = useState("github");
  const searchRef = React.createRef()
  const [repositories, setRepositories] = useState([]);

  const [getRepositories] = useLazyQuery(GET_REPOSITORIES, {
    fetchPolicy: "network-only",
    onCompleted: (data) => updateData(data)
  });
  const [getMoreRepositories] = useLazyQuery(GET_MORE_REPOSITORIES, {
    fetchPolicy: "network-only",
    variables: {
      search: search,
    },
    onCompleted: (data) => addData(data),
  });

  useEffect(() => {
    getRepositories({ variables: { search: search } })
  }, [])

  const updateData = (data: any) => {
    if (data && data.search && data.search.edges) {
      setRepositories(data.search.edges.map(element => {
        return { "cursor": element.cursor, ...element.node }
      }));
    }
  }

  const addData = (data: any) => {
    if (data && data.search && data.search.edges) {
      setRepositories(
        repositories.concat(
          data.search.edges.map(element => {
            return { "cursor": element.cursor, ...element.node }
          })
        )
      );
    }
  }

  const searchRepository = (name: string) => {
    setSearch(name)
    getRepositories({ variables: { search: name } })
  }

  const selectRepository = (repository: Object) => {
    navigation.navigate("RepositoryDetail", repository)
  }

  return (
    <View style={styles.container}>
      <FlatList<any>
        data={repositories}
        ListHeaderComponent={() => {
          return (
            <View style={styles.listHeader}>
              <OutlinedTextField
                ref={searchRef}
                onSubmitEditing={(text: string) => {
                  searchRepository(searchRef.current.value())
                }}
              />
            </View>
          )
        }}
        renderItem={({ item }) => {
          return (
            <View style={styles.listElement}>
              <TouchableOpacity onPress={() => selectRepository(item)}>
                <View style={{ flexDirection: 'row' }}>
                  <Image source={{ uri: item.owner.avatarUrl }} style={styles.listElementOwnerImage} />
                  <Text style={styles.listElementTitle}>{item.nameWithOwner}</Text>
                </View>
                <Text style={styles.listElementDescription}>{item.shortDescriptionHTML}</Text>
              </TouchableOpacity>
            </View>
          )
        }}
        ItemSeparatorComponent={() => {
          return (
            <View style={styles.separator} />
          )
        }}
        ListFooterComponent={() => {
          return (
            <TouchableOpacity onPress={() => {
              if (repositories.length) {
                getMoreRepositories({
                  variables: {
                    search: search,
                    after: repositories[repositories.length - 1].cursor
                  }
                })
              }
            }}
            >
              <Text style={styles.showMore}>Afficher plus</Text>
            </TouchableOpacity>
          )
        }}
        keyExtractor={item => item.nameWithOwner}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  listHeader: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  listElement: {
    paddingHorizontal: 24,
    paddingVertical: 10,
  },
  listElementTitle: {
    fontSize: 15,
    fontWeight: '700',
  },
  listElementDescription: {
    marginTop: 8,
    fontSize: 13,
    fontWeight: '400',
  },
  listElementOwnerImage: {
    width: 50,
    height: 50,
    marginRight: 5,
    marginBottom: 5,
  },
  separator: {
    alignSelf: "center",
    width: "90%",
    borderBottomColor: "black",
    borderWidth: 0.5,
  },
  showMore: {
    paddingVertical: 20,
    margin: 10,
    textAlign: "center",
    backgroundColor: "#dddddd",
    borderColor: "#aaaaaa",
    borderWidth: 1,
  }
});

export default HomeScreen;