import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView
} from 'react-native';


const RepositoryDetailScreen = ({ route }) => {
  const {
    nameWithOwner,
    descriptionHTML,
    owner,
    labels,
  } = route.params;

  return (
    <View style={styles.container}>
      <View>
        <View style={{ flexDirection: 'row' }}>
          <Image source={{ uri: owner.avatarUrl }} style={styles.elementOwnerImage} />
          <Text style={styles.elementTitle}>{nameWithOwner}</Text>
        </View>
        <Text style={styles.elementDescription}>{descriptionHTML}</Text>
        <ScrollView style={{flexDirection: 'row'}} horizontal={true}>
          {
            labels.edges.map((label) => {
              return (
                <Text style={[styles.label, {backgroundColor: "#" + label.node.color}]} key={label.node.name}>{label.node.name}</Text>
              )
            })
          }
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 32,
    paddingHorizontal: 24,
    paddingVertical: 10,
  },
  elementTitle: {
    fontSize: 15,
    fontWeight: '700',
  },
  elementDescription: {
    marginTop: 8,
    fontSize: 13,
    fontWeight: '400',
  },
  elementOwnerImage: {
    width: 50,
    height: 50,
    marginRight: 5,
    marginBottom: 5,
  },
  label: {
    paddingHorizontal: 10,
    marginHorizontal: 2,
    borderRadius: 90,
    borderWidth: 1,
    borderColor: "#aaaaaa"
  },
});

export default RepositoryDetailScreen;